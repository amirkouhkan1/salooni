from django.core.cache import cache

from .selectors.reservations_selectors import get_pending_reservations, get_reservation_by_id
from .services.reservations_services import delete_reservation_by_id

PENDING_TIME = 10 * 10  # 5 minutes = 60 * 10


def generate_prefix(*, reservation_id: int) -> str:
    return f"reservation_{reservation_id}"


def reservation_in_cache(*, reservation_id: int) -> bool:
    return cache.set(generate_prefix(reservation_id=reservation_id), reservation_id, PENDING_TIME)


def remove_from_cache(*, reservation_id: int) -> bool:
    return cache.delete(generate_prefix(reservation_id=reservation_id))


def check_existence_by_id(*, reservation_id: int):
    return cache.get(generate_prefix(reservation_id=reservation_id))


def remove_pending_reservations():
    pending_reservations = get_pending_reservations()

    for pending_reservation in pending_reservations:
        if check_existence_by_id(reservation_id=pending_reservation.id) is None:
            delete_reservation_by_id(reservation_id=pending_reservation.id)
