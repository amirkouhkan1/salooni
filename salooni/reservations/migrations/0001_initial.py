# Generated by Django 4.0.7 on 2023-12-04 10:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('saloons', '0002_saloon_each_section'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(db_index=True, default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('time', models.PositiveSmallIntegerField()),
                ('date', models.DateField()),
                ('status', models.CharField(choices=[('PENDING', 'Pending'), ('RESERVED', 'Reserved'), ('CANCELED', 'Canceled')], default='PENDING', max_length=10)),
                ('saloon', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='saloon', to='saloons.saloon')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
