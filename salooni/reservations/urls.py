from django.urls import path

from salooni.reservations.apis.reservations import CreateReservationView, RemovePendingReservationsView

urlpatterns = [
    path("", CreateReservationView.as_view(), name="create_reservation"),
    path("delete_from_db/", RemovePendingReservationsView.as_view(), name="delete-pending-from-db"),
]
