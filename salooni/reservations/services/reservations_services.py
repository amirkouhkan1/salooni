from datetime import date as Date

from salooni.reservations.models import Reservation
from salooni.saloons.models import Saloon
from salooni.users.models import BaseUser


def create_reservation(*, user: BaseUser, saloon: Saloon, time: int, date: Date) -> Reservation:
    return Reservation.objects.create(user=user, saloon=saloon, time=time, date=date)


def change_status_to_reserved(reservation: Reservation) -> Reservation:
    reservation.status = "RESERVED"
    reservation.save()
    return reservation


def change_status_to_canceled(reservation: Reservation) -> Reservation:
    reservation.status = "CANCELED"
    reservation.save()
    return reservation


def delete_reservation_by_id(*, reservation_id: int):
    return Reservation.objects.filter(id=reservation_id).delete()
