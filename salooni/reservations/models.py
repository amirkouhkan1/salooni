from django.db import models

from salooni.common.models import BaseModel
from salooni.saloons.models import Saloon
from salooni.users.models import BaseUser


class Reservation(BaseModel):
    class Status(models.TextChoices):
        pending = "PENDING"
        reserved = "RESERVED"
        canceled = "CANCELED"

    user = models.ForeignKey(BaseUser, on_delete=models.CASCADE, related_name="user")
    saloon = models.ForeignKey(Saloon, on_delete=models.CASCADE, related_name="saloon")
    time = models.PositiveSmallIntegerField()
    date = models.DateField()
    status = models.CharField(max_length=10, choices=Status.choices, default=Status.pending)

    def __str__(self):
        return f"{self.id}"
