from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..tasks import remove_pending_reservations_from_db
from salooni.apis.mixins import ApiAuthMixin, IsAdminApiAuthMixin
from salooni.reservations.models import Reservation
from salooni.reservations.selectors.reservations_selectors import get_reservation
from salooni.reservations.services.reservations_services import create_reservation
from salooni.reservations.utils import reservation_in_cache, remove_pending_reservations
from salooni.saloons.selectors.saloons import get_saloon


class CreateReservationView(ApiAuthMixin, APIView):
    class CreateReservationInput(serializers.Serializer):
        saloon = serializers.SlugField()
        time = serializers.IntegerField()
        date = serializers.DateField()

    class CreateReservationOutput(serializers.ModelSerializer):
        class Meta:
            model = Reservation
            fields = ("id", "user", "saloon", "time", "date", "created_at", "updated_at", "status")

    def post(self, request):
        serializer = self.CreateReservationInput(data=request.data)
        serializer.is_valid(raise_exception=True)

        if not (saloon := get_saloon(slug=serializer.validated_data["saloon"])):
            return Response(data={"msg": "saloon is not valid"}, status=status.HTTP_400_BAD_REQUEST)

        if not get_reservation(saloon=saloon, time=serializer.validated_data["time"],
                               date=serializer.validated_data["date"]):
            reservation = create_reservation(user=request.user, saloon=saloon, time=serializer.validated_data["time"],
                                             date=serializer.validated_data["date"])
            reservation_in_cache(reservation_id=reservation.id)
            remove_pending_reservations()

            return Response(
                data=self.CreateReservationOutput(reservation).data,
                status=status.HTTP_201_CREATED)
        return Response(data={"msg": "this time is reserved"}, status=status.HTTP_400_BAD_REQUEST)


class RemovePendingReservationsView(APIView):
    def delete(self, request):
        remove_pending_reservations_from_db()
        return Response(data={"msg": "OK"}, status=status.HTTP_204_NO_CONTENT)
