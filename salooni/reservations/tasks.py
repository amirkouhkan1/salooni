from celery import shared_task

from .utils import remove_pending_reservations


@shared_task
def remove_pending_reservations_from_db():
    remove_pending_reservations()
