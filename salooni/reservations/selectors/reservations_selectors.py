from datetime import date as Date

from django.db.models import QuerySet

from salooni.reservations.models import Reservation
from salooni.saloons.models import Saloon
from salooni.users.models import BaseUser


def get_reservation(*, saloon: Saloon, time: int, date: Date) -> Reservation:
    return Reservation.objects.filter(saloon=saloon, time=time, date=date).first()


def user_reservations(*, user: BaseUser) -> QuerySet[Reservation]:
    return Reservation.objects.filter(user=user).all()


def saloon_reservations(*, saloon: Saloon) -> QuerySet[Reservation]:
    return Reservation.objects.filter(saloon=saloon).all()


def get_pending_reservations() -> QuerySet[Reservation]:
    return Reservation.objects.filter(status=Reservation.Status.pending).all()


def get_reservation_by_id(*, reservation_id: int) -> Reservation:
    return Reservation.objects.filter(id=reservation_id).first()
