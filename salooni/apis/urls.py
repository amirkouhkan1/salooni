from django.urls import path, include

urlpatterns = [
    path('auth/', include(('salooni.authentication.urls', 'auth'))),
    path('saloons/', include(('salooni.saloons.urls', 'saloons'))),
    path('reservations/', include(('salooni.reservations.urls', 'reservations'))),
]
