from django.contrib import admin

from salooni.users.models import BaseUser, Profile


class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "phone_number", "role", "created_at")
    list_filter = ("role",)
    search_fields = ("phone_number", "id")


admin.site.register(BaseUser, UserAdmin)

admin.site.register(Profile)
