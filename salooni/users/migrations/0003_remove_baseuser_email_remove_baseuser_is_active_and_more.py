# Generated by Django 4.0.7 on 2023-11-27 16:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_profile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='baseuser',
            name='email',
        ),
        migrations.RemoveField(
            model_name='baseuser',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='baseuser',
            name='is_admin',
        ),
        migrations.AddField(
            model_name='baseuser',
            name='phone_number',
            field=models.CharField(default=0, max_length=12, unique=True),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Profile',
        ),
    ]
