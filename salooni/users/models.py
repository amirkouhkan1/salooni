import enum

from django.db import models
from salooni.common.models import BaseModel

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager as BUM
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _


class BaseUserManager(BUM):
    def create_user(self, phone_number: str, password=None):
        if not phone_number:
            raise ValueError("Users must have an Phone Number")

        user = self.model(phone_number=phone_number)

        if password is not None:
            user.set_password(password)
        else:
            user.set_unusable_password()

        user.full_clean()
        user.save(using=self._db)

        return user

    def create_superuser(self, phone_number: str, password: str = None):
        user = self.create_user(
            phone_number=phone_number,
            password=password,
        )
        user.role = Roles.ADMIN.value
        user.save(using=self._db)

        return user


class Roles(models.Choices):
    USER = 0
    ADMIN = 1


class BaseUser(BaseModel, AbstractBaseUser, PermissionsMixin):
    phone_number = models.CharField(_("Phone Number"), max_length=12, unique=True)
    role = models.PositiveSmallIntegerField(_("Role of User"), default=Roles.USER.value)

    objects = BaseUserManager()

    USERNAME_FIELD = "phone_number"

    def is_staff(self):
        return self.role == Roles.ADMIN.value

    def is_admin(self):
        return self.role == Roles.ADMIN.value

    def __str__(self):
        return self.phone_number


class Profile(BaseModel):
    user = models.OneToOneField(BaseUser, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=64, null=True, blank=True)
    card_id = models.CharField(max_length=10, null=True, blank=True)
    card_photo = models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):
        return f"{self.user}"
