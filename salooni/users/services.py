from django.db import transaction
from .models import BaseUser, Profile


def create_profile(*, user: BaseUser, full_name: str | None, card_id: str | None, card_photo: str | None) -> Profile:
    return Profile.objects.create(user=user, full_name=full_name, card_id=card_id, card_photo=card_photo)


def authenticate_user(*, phone_number: str) -> BaseUser:
    return BaseUser.objects.get_or_create(
        phone_number=phone_number, defaults={"phone_number": phone_number}
    )
