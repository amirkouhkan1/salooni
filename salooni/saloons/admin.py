from django.contrib import admin

from salooni.saloons.models import Saloon, SaloonFeature, SaloonType


@admin.register(Saloon)
class SaloonAdmin(admin.ModelAdmin):
    list_display = ("name", "slug", "owner", "province", "city", "created_at")
    list_filter = ("city", "province", "active")
    search_fields = ("name", "city", "province")
    prepopulated_fields = {"slug": ("name",)}


@admin.register(SaloonFeature)
class SaloonFeaturesAdmin(admin.ModelAdmin):
    list_display = ("title", "created_at")
    search_fields = ("title",)
    prepopulated_fields = {"slug": ("title",)}


@admin.register(SaloonType)
class SaloonTypeAdmin(admin.ModelAdmin):
    list_display = ("title", "created_at")
    search_fields = ("title",)
    prepopulated_fields = {"slug": ("title",)}
