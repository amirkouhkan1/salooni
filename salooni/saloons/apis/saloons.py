from django.urls import reverse
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters import rest_framework as filters
from django.contrib.postgres.search import SearchVector

from salooni.apis.pagination import LimitOffsetPagination, get_paginated_response_context
from salooni.saloons.models import Saloon
from salooni.saloons.selectors.saloons import get_saloons, get_saloon


class SaloonView(APIView):
    class Pagination(LimitOffsetPagination):
        default_limit = 10

    class FilterSerializer(serializers.Serializer):
        name = serializers.CharField(required=False, max_length=64)

    class SaloonOutputSerializer(serializers.ModelSerializer):
        url = serializers.SerializerMethodField("get_url")

        class Meta:
            model = Saloon
            exclude = ("updated_at",)

        def get_url(self, saloon):
            request = self.context.get("request")
            path = reverse("apis:saloons:saloon_detail", args=(saloon.slug,))
            return request.build_absolute_uri(path)

    @staticmethod
    def apply_filters(queryset, filters):
        if filters.get("name"):
            queryset = queryset.filter(name__icontains=filters["name"])
        return queryset

    def get(self, request):
        filter_serializer = self.FilterSerializer(data=request.query_params)
        filter_serializer.is_valid(raise_exception=True)

        queryset = get_saloons()
        queryset = self.apply_filters(queryset, filter_serializer.validated_data)

        return get_paginated_response_context(
            pagination_class=LimitOffsetPagination,
            serializer_class=self.SaloonOutputSerializer,
            queryset=queryset,
            request=request,
            view=self
        )


class SaloonDetailView(APIView):
    class Pagination(LimitOffsetPagination):
        default_limit = 10

    class SaloonOutputSerializer(serializers.ModelSerializer):
        available_times = serializers.SerializerMethodField("get_available_times")

        def get_available_times(self, saloon):
            times = [saloon.start_time * 1.0]
            for time in range(saloon.start_time, saloon.end_time):
                times.append(time + saloon.each_section)
            return times

        class Meta:
            model = Saloon
            exclude = ("updated_at",)

    def get(self, request, slug):
        saloon = get_saloon(slug=slug)
        if not saloon:
            return Response(data={"msg": "Does Not exist"}, status=status.HTTP_404_NOT_FOUND)
        serializer = self.SaloonOutputSerializer(saloon)
        return Response(serializer.data)
