from django.db.models import QuerySet

from salooni.saloons.models import Saloon


def get_saloons() -> QuerySet[Saloon]:
    return Saloon.objects.all()


def get_saloon(slug: str) -> Saloon:
    return Saloon.objects.filter(slug=slug).first()
