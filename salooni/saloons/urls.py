from django.urls import path

from salooni.saloons.apis.saloons import SaloonView, SaloonDetailView

urlpatterns = [
    path("", SaloonView.as_view(), name="saloons"),
    path("<str:slug>/", SaloonDetailView.as_view(), name="saloon_detail"),
]