from django.db import models
from django.utils.translation import gettext_lazy as _

from salooni.common.models import BaseModel
from salooni.users.models import BaseUser


class SaloonFeature(BaseModel):
    title = models.CharField(max_length=64)
    slug = models.CharField(max_length=64, unique=True, db_index=True)
    description = models.CharField(max_length=512)
    icon = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.title


class SaloonType(BaseModel):
    title = models.CharField(max_length=64)
    slug = models.CharField(max_length=64, unique=True, db_index=True)
    description = models.CharField(max_length=512)
    icon = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.title


class Saloon(BaseModel):
    owner = models.ForeignKey(BaseUser, verbose_name=_("Owner of saloon"), on_delete=models.CASCADE,
                              related_name="owner")
    name = models.CharField(max_length=64)
    slug = models.SlugField(max_length=64, unique=True, db_index=True)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    province = models.CharField(max_length=64)
    city = models.CharField(max_length=64)
    address = models.CharField(max_length=512)
    start_time = models.IntegerField()
    end_time = models.IntegerField()
    each_section = models.FloatField()
    active = models.BooleanField(default=False)
    features = models.ManyToManyField(SaloonFeature, related_name="features")
    type = models.ForeignKey(SaloonType, on_delete=models.CASCADE, related_name="type")

    class Meta:
        unique_together = (
            ("latitude", "longitude"),
        )

    def __str__(self):
        return self.name
