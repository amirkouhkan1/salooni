from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers, status
from rest_framework_simplejwt.tokens import RefreshToken

from salooni.authentication.tasks import send_token_to_user
from salooni.authentication.utils import user_token_in_cache, verify_user_token
from salooni.users.models import BaseUser
from salooni.users.services import authenticate_user, create_profile


class AuthenticationView(APIView):
    class AuthenticationInput(serializers.Serializer):
        phone_number = serializers.CharField(min_length=10, max_length=10)

    class AuthenticationOutput(serializers.ModelSerializer):
        phone_number = serializers.CharField(min_length=10, max_length=10)

    def post(self, request):
        serializer = self.AuthenticationInput(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = serializer.validated_data["phone_number"]
        token = user_token_in_cache(phone_number=phone_number)
        send_token_to_user(phone_number=phone_number, token=token)
        return Response(serializer.data)


class VerifyAuthenticationView(APIView):
    class VerifyAuthenticationInput(serializers.Serializer):
        phone_number = serializers.CharField(min_length=10, max_length=10)
        token = serializers.CharField(min_length=4, max_length=4)

    class VerifyAuthenticationOutput(serializers.ModelSerializer):
        def generate_token(self, user):
            refresh = RefreshToken.for_user(user)
            access = refresh.access_token

            return {"refresh": str(refresh), "access": str(access)}

        token = serializers.SerializerMethodField("generate_token")

        class Meta:
            model = BaseUser
            fields = ("id", "phone_number", "role", "token",  "created_at", "updated_at")

    def post(self, request):
        serializer = self.VerifyAuthenticationInput(data=request.data)
        serializer.is_valid(raise_exception=True)

        phone_number = serializer.validated_data["phone_number"]
        token = serializer.validated_data["token"]

        match (verify_user_token(phone_number=phone_number, token=token)):
            case 0:
                return Response(
                    data={"msg": "Token or Phone Number is not exists"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            case 2:
                return Response(
                    data={"msg": "Token is not matched"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            case 1:
                user, created = authenticate_user(phone_number=phone_number)
                if not created:
                    return Response(
                        self.VerifyAuthenticationOutput(user).data,
                        status=status.HTTP_200_OK,
                    )
                create_profile(
                    user=user, full_name=None, card_photo=None, card_id=None
                )
                return Response(
                    self.VerifyAuthenticationOutput(user).data,
                    status=status.HTTP_201_CREATED,
                )
