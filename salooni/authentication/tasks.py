from celery import shared_task


@shared_task
def send_token_to_user(*, phone_number: str, token: int) -> bool:
    print(f"{phone_number} -> {token}")
    return True
