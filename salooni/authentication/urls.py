from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

from salooni.authentication.apis.auth import AuthenticationView, VerifyAuthenticationView

urlpatterns = [
    path("", AuthenticationView.as_view(), name="auth"),
    path("verify/", VerifyAuthenticationView.as_view(), name="verify"),
]
