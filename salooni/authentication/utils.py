from datetime import timedelta
from random import randint
from typing import Optional

from django.core.cache import cache

CACHE_TIMEOUT = 60 * 2


def generate_prefix(*, phone_number: str) -> str:
    return f"auth_{phone_number}"


def generate_token() -> int:
    return randint(1000, 9999)


def user_token_in_cache(*, phone_number: str) -> int:
    if not cache.get(generate_prefix(phone_number=phone_number)):
        token = generate_token()
        cache.set(generate_prefix(phone_number=phone_number), token, CACHE_TIMEOUT)
        return token
    return cache.get(generate_prefix(phone_number=phone_number))


def verify_user_token(*, phone_number: str, token: int) -> int:
    """ 0 -> Token for user does not exist.
        1 -> Verified
        2 -> Input token is not match with token in cache
    """
    user_in_cache = cache.get(generate_prefix(phone_number=phone_number))

    if not user_in_cache:
        return 0
    elif int(user_in_cache) == int(token):
        cache.delete(generate_prefix(phone_number=phone_number))
        return 1
    else:
        return 2
